export default {
    name: "projekcije",
    data() {
        return {
            projekcije: [],
            filmici: []
        }
    },
    mounted(){
        this.dobaviFilmove();
        this.dobaviProjekcije();
        
    },
    methods:{
        dobaviProjekcije(){
            axios.get("/api/projekcije").then(response => {
                this.projekcije = response.data;
            });
        },
        dobaviFilmove(){
            axios.get("/api/filmovi").then(response => {
                this.filmici = response.data;
            });
        },
        ispravkaDatuma(date) {
            const d = new Date(date);
            const dan = String(d.getDate());
            const mesec = String(d.getMonth() + 1);
            const godina = String(d.getFullYear());
            return dan + "/" + mesec+ "/" + godina;
        },
        pronadjiNazivFilma(film_id) {
            let film = this.filmici.find(film => film.idFilm === film_id);
            if(film){
                return film.naziv
            }
            else{
                return "Nepoznat Film"
            }
        },

    },
    template: `
        <h1>Projekcije filmova</h1>
        <table>
            <thead>
                <tr>
                    <th>Film</th>
                    <th>Termin</th>
                    <th>Cena</th>
                 </tr>
            </thead>
            <tbody>
                <tr v-for="projekcija in projekcije" :key="projekcija.idProjekcija">
                    <td>{{pronadjiNazivFilma(projekcija.film_id)}}</td>
                    <td>{{ispravkaDatuma(projekcija.termin)}}</td>
                    <td>{{projekcija.cena}}</td>
            </tr>
            </tbody>
        </table>
        <br>
        <h1>Filmovi u nasoj ponudi</h1>
        <table>
        <thead>
            <tr>
                <th>Naziv</th>
                <th>Zanr</th>
                <th>Premijera</th>
                <th>Reziser</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="film in filmici" :key="film.idFilm">
                <td>{{film.naziv}}</td>
                <td>{{film.zanr}}</td>
                <td>{{ispravkaDatuma(film.premijera)}}</td>
                <td>{{film.reziser}}</td>
         </tr>
        </tbody>
        </table>
    `,

}