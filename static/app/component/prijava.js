export default {
    name: "prijava",
    data(){
        return{
            korisnikLogin: [],
            proveraKorisnika: [],
            proveraAdmina: [],
            
        }
    },
    created(){
        this.dobaviUser();
        this.dobaviAdmina();
    },
    methods:{
        dobaviUser(){
            axios.get("/api/korisnici").then(response => {
                this.proveraKorisnika = response.data;
            });
        },
        dobaviAdmina(){
            axios.get("/api/administratore").then(response => {
                this.proveraAdmina = response.data;
            });
        },
        prijava(){
            //axios.post("/api/login", this.korisnikLogin)
            const { korisnicko_ime, password, role } = this.korisnikLogin;

            if (role === "user" || role === "admin") {
                const isUserMatch = this.proveraKorisnika.some(
                    (korisnik) =>
                        korisnik.korisnicko_ime === korisnicko_ime &&
                        korisnik.password === password
                );
                const isAdminMatch = this.proveraAdmina.some(
                    (admin) =>
                        admin.korisnicko_ime === korisnicko_ime &&
                        admin.password === password
                );
                if (isUserMatch && role === "user") {
                    this.$router.push("/korisnickastrana");
                } else if (isAdminMatch && role === "admin") {
                    this.$router.push("/adminStrana");
                } else {
                    this.$router.push("/login");
                    window.location.reload();
                }
            }
        }
    },
    template: `
        <div>
        <h1>Prijava</h1>
        <p>korisnicko_ime: <b>admi</b> Password: <b>admi</b> role: <b>Admin</b> (za Admin Stranu)</p>
        <p>korisnicko_ime: <b>korisnik</b> Password: <b>korisnik</b> role: <b>User</b> (za Korisnik Stranu)</p>
        <form name="prijava" @submit.prevent="prijava" method="post">
            <input type="text" id="korisnicko_ime" v-model="korisnikLogin.korisnicko_ime" placeholder="Ime" style="margin-right: 10px" required>
            <input type="password" id="password" v-model="korisnikLogin.password" placeholder="Sifra" style="margin-right: 10px" required>
            <label for="role">Role : </label>
            <select id="role" v-model="korisnikLogin.role" required> 
                <option value="user">User</option>
                <option value="admin">Admin</option>
            </select>
            <button type="submit">Login</button>
        </form>
        </div>
      `,    
}