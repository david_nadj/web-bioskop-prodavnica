export default {
    name: "korisnickastrana",
    data() {
        return {
            projekcije: [],
            filmici: [],
            karte: [],
            novaProjekcija: {
                admin_id: 1,
            },
        }
    },
    mounted(){
        this.dobaviProjekcije();
        this.dobaviFilmove();
        this.dobaviKarte();
    },
    methods:{
        dobaviProjekcije(){
            axios.get("/api/projekcije").then(response => {
                this.projekcije = response.data;
            });
        },
        dobaviFilmove(){
            axios.get("/api/filmovi").then(response => {
                this.filmici = response.data;
            });
        },
        dobaviKarte(){
            axios.get("/api/karte").then(response => {
                this.karte = response.data;
                console.log(this.karte);
            });
        },
        ispravkaDatuma(date) {
            const d = new Date(date);
            const dan = String(d.getDate());
            const mesec = String(d.getMonth() + 1);
            const godina = String(d.getFullYear());
            return dan + "/" + mesec+ "/" + godina;
        },
        pronadjiNazivFilma(film_id) {
            let film = this.filmici.find(film => film.idFilm === film_id);
            
            if(film){
                return film.naziv
            }
            else{
                return "Nepoznat Film"
            }
        },
        kupiKartu(){
            console.log("sta se salje: ", this.novaProjekcija);
            axios
                .post("/api/karta", this.novaProjekcija)
                .then(response => {this.$router.push({path:"/korisnickaStrana"});});
            console.log("sta se salje:", this.novaProjekcija);
        }

    },
    template: `
        <h3>Projekcije</h3>
        <table>
            <thead>
                <tr>
                    <th>Film</th>
                    <th>Termin</th>
                    <th>Cena</th>
                    <th>AKCIJA</th>
                 </tr>
            </thead>
            <tbody>
                <tr v-for="projekcija in projekcije" :key="projekcija.idProjekcija">
                    <td>{{pronadjiNazivFilma(projekcija.film_id)}}</td>
                    <td>{{ispravkaDatuma(projekcija.termin)}}</td>
                    <td>{{projekcija.cena}}</td>
                    <td>
                    <button @click.prevent="kupiKartu">Kupi Kartu</button>
                    </td>
            </tr>
            </tbody>
        </table>
        <br>
        <h3>Karte</h3>
        <table>
            <thead>
                <tr>
                    <th>Korisnik</th>
                    <th>film</th>
                    <th>Termin</th>
                    <th>Datum Kupovine</th>
                    <th>Cena</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="karta in karte" :key="karta.idkarte">
                    <td>{{karta.korisnicko_ime}}</td>
                    <td>{{karta.naziv}}</td>
                    <td>{{ispravkaDatuma(karta.termin)}}</td>
                    <td>{{ispravkaDatuma(karta.datum_kupovine)}}</td>
                    <td>{{karta.cena}}</td>
            </tr>
            </tbody>
        </table>
    `,

}