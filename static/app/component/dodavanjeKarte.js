export default {
    name:"dodavanjefilma",
    data() {
        return {
            novaKarta: {
                admin_id: 1,
            },
            kartice: [],
            korisnici: [],
            projekcije: [],
            filmici: [],
        }
    },
    mounted(){
        this.dobaviKarte();
        this.dobaviFilmove();
        this.dobaviKarte();
        this.dobaviKorisnike();
        this.dobaviProjekcije();
    },

    methods: {
        dodavanjeKarta(){
            console.log("sta se salje: ", this.novaKarta);
            axios
                .post("/api/karta", this.novaKarta)
                .then(response => {this.$router.push({path:"/adminstrana"});});
            console.log("sta se salje:", this.novaKarta);
        },
        izmenaKarte(idKarte){
            console.log("sta se salje: ", this.novaKarta);
            axios
                .put(`/api/karta/${idKarte}`, this.novaKarta)
                .then(response => {this.$router.push({path:"/adminstrana"});});
            console.log("sta se salje:", this.novaKarta);
        },
        sacuvaj(){
            if (this.$route.params.idKarte) {
                this.izmenaKarte(this.$route.params.idKarte);
              } else {
                this.dodavanjeKarta();
              }
        },
        dobaviFilmove(){
            axios.get("/api/filmovi").then(response => {
                this.filmici = response.data;
            });
        },
        dobaviKarte(){
            axios.get("/api/karte").then(response => {
                this.kartica = response.data;
            });
        },
        dobaviKorisnike(){
            axios.get("/api/korisnici").then(response => {
                this.korisnici = response.data;
            });
        },
        dobaviProjekcije(){
            axios.get("/api/projekcije").then(response => {
                this.projekcije = response.data;
            });
        },
    },

    template: `
    <div>
    <h1 v-if="$route.params.idKarte">Izmeni Kartu</h1>
    <h1 v-else>Dodaj Kartu</h1>
    <form name="sacuvaj" @submit.prevent="sacuvaj" method="post">

        <label for="film_id">Ime Filma: </label>

        <select name="film_id" id="film_id" v-model="projekcije.film_id"   required> <br>
            <option v-for="film in filmici" :key="film.idFilm" :value="film.idFilm">{{ film.naziv }}</option>
        </select> <br>

        <label for="korisnik_id">Ime korisnika: </label>

        <select name="korisnik_id" id="korisnik_id" v-model="novaKarta.korisnik_id" required>
            <option v-for="korisnik in korisnici" :key="korisnik.idKorisnik" :value="korisnik.idKorisnik">{{korisnik.korisnicko_ime}}</option>
        </select> <br>

        <label for="datum_kupovine">Datum kupovine: </label>
        <input type="date" name="datum_kupovine" id="datum_kupovine" v-model="novaKarta.datum_kupovine"> <br>
        <label for="cena">Cena: </label>
        <input type="text" name="cena" id="cena" v-model="novaKarta.cena" required> <br>
        <button type="submit" v-if="$route.params.idKarte">Izmeni Kartu</button>
        <button type="submit" v-else>Dodaj Kartu</button>  
    </form>
    </div>
        `
}