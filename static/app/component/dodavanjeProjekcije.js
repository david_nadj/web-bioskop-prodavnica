export default {
    name:"dodavanjeprojekcije",
    data() {
        return {
            novaProjekcija: {},
            filmici: [],
            jednaProjekcija: [],
        }
    },
    mounted(){
        this.dobaviFilmove();
    },

    methods: {
        dodavanjeProjekcije(){
            console.log("sta se salje: ", this.novaProjekcija);
            axios
                .post("/api/projekcija", this.novaProjekcija)
                .then(response => {this.$router.push({path:"/adminstrana"});});
            console.log("sta se salje:", this.novaProjekcija);
        },
        izmenaProjekcije(idProjekcija){
            console.log("sta se salje: ", this.novaProjekcija);
            axios
                .put(`/api/projekcija/${idProjekcija}`, this.novaProjekcija)
                .then(response => {this.$router.push({path:"/adminstrana"});});
            console.log("sta se salje:", this.novaProjekcija);
        },
        sacuvaj(){
            if (this.$route.params.idProjekcija) {
                this.izmenaProjekcije(this.$route.params.idProjekcija);
                console.log("HELP2")
              } else {
                this.dodavanjeProjekcije();
                console.log("HELP1")
              }
        },
        dobaviFilmove(){
            axios.get("/api/filmovi").then(response => {
                this.filmici = response.data;
            });
        },
        dobaviProjekciju(idProjekcija) {
            axios.get(`/api/projekcija/${idProjekcija}`).then((response) => {
              this.jednaProjekcija = response.data;
            });
        },
    },

    template: `
        <div>
        <form name="sacuvaj" @submit.prevent="sacuvaj" method="post">
            <h1 v-if="$route.params.idProjekcija">Izmeni projekciju</h1>
            <h1 v-else>Dodaj Projekciju</h1>
            <label for="film_id">Ime Filma: </label>
            <select name="film_id" id="film_id" v-model="novaProjekcija.film_id"   required> <br>
                <option v-for="film in filmici" :key="film.idFilm" :value="film.idFilm">{{ film.naziv }}</option>
            </select> <br>
            <label for="termin">termin: </label>
            <input type="date" name="termin" id="termin" v-model="novaProjekcija.termin" required> <br>
            <label for="cena">Cena: </label>
            <input type="text" name="cena" id="cena" v-model="novaProjekcija.cena" required> <br>
            <button type="submit" v-if="$route.params.idProjekcija">Izmeni Projekciju</button>
            <button type="submit" v-else>Dodaj Projekciju</button>
        </form>
        </div>
        `
}