export default {
    name: "administrativnastrana",
    data() {
        return {
            projekcije: [],
            filmici: [],
            korisnici: [],
            karte: [],
        }
    },
    mounted(){
        this.dobaviProjekcije();
        this.dobaviFilmove();
        this.dobaviKorisnike();
        this.dobaviKarte();
    },
    methods:{
        dobaviProjekcije(){
            axios.get("/api/projekcije").then(response => {
                this.projekcije = response.data;
            });
        },
        dobaviFilmove(){
            axios.get("/api/filmovi").then(response => {
                this.filmici = response.data;
            });
        },
        dobaviKorisnike(){
            axios.get("/api/korisnici").then(response => {
                this.korisnici = response.data;
            });
        },
        dobaviKarte(){
            axios.get("/api/karte").then(response =>{
                this.karte = response.data;
            })
        },
        ispravkaDatuma(date) {
            const d = new Date(date);
            const dan = String(d.getDate());
            const mesec = String(d.getMonth() + 1);
            const godina = String(d.getFullYear());
            return dan + "/" + mesec+ "/" + godina;
        },
        pronadjiNazivFilma(film_id) {
            let film = this.filmici.find(film => film.idFilm === film_id);
            
            if(film){
                return film.naziv;
            }
            else{
                return "Nepoznat Film"
            };
        },
        izmeniProjekciju(idProjekcija){
            this.$router.push({path:"/dprojekcije/" + idProjekcija});
        },
        izmeniFilm(idFilm){
            this.$router.push({path:"/dfilma/" + idFilm});
        },
        izmenaKorisnika(idKorisnik){
            this.$router.push({path:"/register/" + idKorisnik});
        },
        izmenaKarte(idKarte){
            this.$router.push({path:"/dkarte/" + idKarte})
        },
        brisanjeFilma(idFilm){
            axios.delete(`/api/film/${idFilm}`).then(response => {window.location.reload()});

        },
        brisanjeKorisnika(idKorisnik){
            axios.delete(`/api/korisnik/${idKorisnik}`).then(response => {window.location.reload()});
        },
        brisanjeProjekcije(idProjekcija){
            console.log("izbrisano")
            axios.delete(`/api/projekcija/${idProjekcija}`).then(response => {window.location.reload()});
        },
        brisanjeKarte(idKarte){
            console.log(this.karte)
            axios.delete(`api/karta/${idKarte}`).then(response => {window.location.reload()});
        }

    },
    template: `
        <h3>Projekcije</h3>
        <table>
            <thead>
                <tr>
                    <th>Film</th>
                    <th>Termin</th>
                    <th>Cena</th>
                    <th>AKCIJA</th>
                 </tr>
            </thead>
            <tbody>
                <tr v-for="projekcija in projekcije" :key="projekcija.idProjekcija">
                    <td>{{pronadjiNazivFilma(projekcija.film_id)}}</td>
                    <td>{{ispravkaDatuma(projekcija.termin)}}</td>
                    <td>{{projekcija.cena}}</td>
                    <td>

                    <button @click="izmeniProjekciju(projekcija.idProjekcija)">Izmeni</button>
                    <button @click="brisanjeProjekcije(projekcija.idProjekcija)">Brisanje</button>
                    </td>
            </tr>
            </tbody>
        </table>
        <button @click="this.$router.push({path:'/dprojekcije'})">Dodaj</button>
        <br>
            <h3>Korisnici</h3>
            <table>
                <thead>
                    <tr>
                        <th>Korisnicko Ime</th>
                        <th>Sifra</th>
                        <th>Ime</th>
                        <th>Uloga</th>
                        <th>AKCIJA</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="korisnik in korisnici" :key="korisnik.idKorisnik">
                        <td>{{korisnik.korisnicko_ime}}</td>
                        <td>{{korisnik.password}}</td>
                        <td>{{korisnik.ime}}</td>
                        <td>{{korisnik.role}}</td>
                        <td>

                        <button @click="izmenaKorisnika(korisnik.idKorisnik)">Izmeni</button>
                        <button @click="brisanjeKorisnika(korisnik.idKorisnik)">Brisanje</button>
                        </td>
                </tr>
                </tbody>
            </table>
            <button @click.prevent="this.$router.push({path:'/register'})" >Dodaj</button>
            <br>
            <h3>Filmovi</h3>
            <table>
                <thead>
                    <tr>
                        <th>Naziv</th>
                        <th>Zanr</th>
                        <th>Premijera</th>
                        <th>Reziser</th>
                        <th>Akcija</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="film in filmici" :key="film.idFilm">
                        <td>{{film.naziv}}</td>
                        <td>{{film.zanr}}</td>
                        <td>{{ispravkaDatuma(film.premijera)}}</td>
                        <td>{{film.reziser}}</td>
                        <td>
                        <button @click="izmeniFilm(film.idFilm)">Izmeni</button>
                        <button @click="brisanjeFilma(film.idFilm)">Brisanje</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <button @click="this.$router.push({path:'/dfilma'})">Dodaj</button>
            <br>
            <h3>Karte</h3>
            <table>
                <thead>
                    <tr>
                        <th>Korisnik</th>
                        <th>film</th>
                        <th>Termin</th>
                        <th>Datum Kupovine</th>
                        <th>Cena</th>
                        <th>Termin</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="karta in karte" :key="karta.idkarte">
                        <td>{{karta.korisnicko_ime}}</td>
                        <td>{{karta.naziv}}</td>
                        <td>{{ispravkaDatuma(karta.termin)}}</td>
                        <td>{{ispravkaDatuma(karta.datum_kupovine)}}</td>
                        <td>{{karta.cena}}</td>
                        <td>
                        <button @click="izmenaKarte(karta.idKarte)" >Izmeni</button>
                        <button @click="brisanjeKarte(karta.idKarte)">Brisanje</button>
                        </td>
                </tr>
                </tbody>
            <button @click="this.$router.push({path:'/dkarte'})">Dodaj</button>
            </table>
    `,

}