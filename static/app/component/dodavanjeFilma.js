export default {
    name:"dodavanjefilma",
    data() {
        return {
            noviFilm: {},
            film: [],
        }
    },
    mounted(){
        this.dobaviFilmove;
    },

    methods: {
        dodavanjeFilma(){
            console.log("sta se salje: ", this.noviFilm);
            axios
                .post("/api/film", this.noviFilm)
                .then(response => {this.$router.push({path:"/adminstrana"});});
            console.log("sta se salje:", this.noviFilm);
        },
        izmenaFilma(idFilm){
            console.log("sta se salje: ", this.noviFilm);
            axios
                .put(`/api/film/${idFilm}`, this.noviFilm)
                .then(response => {this.$router.push({path:"/adminstrana"});});
            console.log("sta se salje:", this.noviFilm);
        },
        sacuvaj(){
            if (this.$route.params.idFilm) {
                this.izmenaFilma(this.$route.params.idFilm);
              } else {
                this.dodavanjeFilma();
              }
        },
        dobaviFilm(idFilm){
            axios.get(`/api/film/${idFilm}`).then(response => {
                this.filmici = response.data;
            });
        },
    },

    template: `
        <div>
        <h1 v-if="$route.params.idFilm">Izmeni Film</h1>
        <h1 v-else>Dodaj Film</h1>
        <form name="sacuvaj" @submit.prevent="sacuvaj" method="post">
            <label for="naziv">Naziv filma: </label>
            <input type="text" name="naziv" id="naziv" v-model="noviFilm.naziv" required> <br>
            <label for="zanr">Zanr: </label>
            <input type="text" name="zanr" id="zanr" v-model="noviFilm.zanr" required> <br>
            <label for="premijera">Premijera: </label>
            <input type="date" name="premijera" id="premijera" v-model="noviFilm.premijera" required> <br>
            <label for="reziser">Reziser: </label>
            <input type="text" name="reziser" id="reziser" v-model="noviFilm.reziser" required> <br>
            <button type="submit" v-if="$route.params.idFilm">Izmeni Film</button>
            <button type="submit" v-else>Dodaj Film</button>  
        </form>
        </div>
        `
}