export default {
    name: "registracija",
    data(){
        return{
            noviKorisnik: {},
            korisnici: [],
        }
    },
    methods:{
        dobaviKorisnike(){
            axios.get("/api/korisnici").then(response => {
                this.korisnici = response.data;
            });
        },
        dodavanjeKorisnika(){
            console.log("sta se salje: ", this.noviKorisnik);
            axios
                .post("/api/register", this.noviKorisnik)
                .then(response => {this.$router.push({path:"/login"});});
            console.log("sta se salje:", this.noviKorisnik);
        },
        prviAdmin(){
            if (this.noviKorisnik.role === "user") {
            this.noviKorisnik.admin_id = "1";
            }
        },
        izmenaKorisnika(idKorisnik){
            console.log("sta se salje: ", this.noviKorisnik);
            axios
                .put(`/api/korisnik/${idKorisnik}`, this.noviKorisnik)
                .then(response => {this.$router.push({path:"/adminstrana"});});
            console.log("sta se salje:", this.noviKorisnik);
        },
        promena(){
            if (this.$route.params.idKorisnik) {
                this.izmenaKorisnika(this.$route.params.idKorisnik);
                console.log("HELP2")
              } else {
                this.dodavanjeKorisnika();
                console.log("HELP1")
              }
        },
    },
    template: `
        <div>
        <h1 v-if="$route.params.idKorisnik">Izmeni korisnika</h1>
        <h1 v-else>Registracija korisnika I admina</h1>
        <form name="promena" @submit.prevent="promena" method="post">
            <label >Korisnicko Ime: </label>
            <input type="text" name="korisnicko_ime" id="korisnicko_ime" v-model="noviKorisnik.korisnicko_ime" required> <br>
            <label for="password">Sifra: </label>
            <input type="text" name="password" id="password" v-model="noviKorisnik.password" required> <br>
            <label for="ime">Ime: </label>
            <input type="text" name="ime" id="ime" v-model="noviKorisnik.ime" required> <br>
            <label for="role">Uloga: </label>
            <input v-if="$route.params.idKorisnik" type="text" name="role" id="role" v-model="noviKorisnik.role" required>
            <select v-else id="role" v-model="noviKorisnik.role"  required @change="prviAdmin">
            <option value="user">User</option>
            <option value="admin">Admin</option>
            </select> <br>


            <button type="submit" v-if="$route.params.idKorisnik">Izmeni Korisnika</button>
            <button type="submit" v-else>Registruj se</button>
            
        </form>
        </div>
        `,
    
}