import Prijava from "./component/prijava.js";
import Projekcije from "./component/projekcije.js";
import Registracija from "./component/registracija.js";
import KorisnickaStrana from "./component/korisnickaStrana.js"
import AdministrativnaStrana from "./component/adminStrana.js"

import dodavanjeProjekcije from "./component/dodavanjeProjekcije.js";
import dodavanjeFilma from "./component/dodavanjeFilma.js";
import pretraga from "./component/pretraga.js";
import dodavanjeKarte from "./component/dodavanjeKarte.js";

const routes = [
  { path: "/", name: "pocetna", component: Projekcije},
  { path: "/projekcije", name: "projekcije", component: Projekcije},
  { path: "/login", name: "prijava", component: Prijava},
  { path: "/register", name: "registracija", component: Registracija},
  { path: "/register/:idKorisnik", name: "izmena", component: Registracija},
  { path: "/korisnickastrana", name: "korisnickastrana", component: KorisnickaStrana},
  { path: "/adminstrana", name: "administrativnastrana", component: AdministrativnaStrana},
  { path: "/dkarte", name: "dodavanjekarte", component: dodavanjeKarte},
  { path: "/dkarte/:idKarte", name: "izmenakarta", component: dodavanjeKarte},
  { path: "/dprojekcije", name: "dodavanjeprojekcije", component: dodavanjeProjekcije},
  { path: "/dprojekcije/:idProjekcija", name: "izmenaprojekcije", component: dodavanjeProjekcije},
  { path: "/dfilma", name:"dodavanjefilma", component: dodavanjeFilma},
  { path: "/dfilma/:idFilm", name:"izmenafilma", component: dodavanjeFilma},
  { path: "/pretraga", name:"pretraga", component: pretraga},
]

const router = VueRouter.createRouter({
  history: VueRouter.createWebHashHistory(),
  routes
})
const app = Vue.createApp({
  data(){
    return {
      podatak: [],
      podatakPretrage: [],
      projekcije: []

    }

  },
  methods: {
    pretrazivanje(){
      console.log("ovo trazis1 PodatakPretrage ", this.podatakPretrage)
      console.log("ovo trazis2 podatak", this.podatak)
      axios.get(`/api/pretraga?q=${this.podatak}`)
        .then(response => {
          this.podatakPretrage = response.data;
          console.log("ovo trazis3 pretrage", this.podatakPretrage)
          console.log("ovo trazis4 podatak", this.podatak)
        })
      console.log("ovo trazis5 pretrazivanje", this.podatakPretrage)
      console.log("ovo trazis6 ", this.podatak)
    },
  }, 

})

app.use(router)
app.mount('#app');