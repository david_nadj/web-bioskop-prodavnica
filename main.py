import flask
from flask import Flask
from flaskext.mysql import MySQL
from flaskext.mysql import pymysql
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
import functools
import datetime

app = Flask(__name__, static_url_path="")
app.secret_key = 'MOJKLJUC'

mysql = MySQL(cursorclass=pymysql.cursors.DictCursor)

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "1234"
app.config["MYSQL_DATABASE_DB"] = "webioskop"
app.config["MYSQL_DATABASE_HOST"] = "localhost"

mysql.init_app(app)

@app.route("/")
@app.route("/index")
@app.route("/index.html")
def index_page():
    return app.send_static_file("index.html")


@app.route("/api/pretraga")
def pretrazivanje():
    rezultatPretrage = request.args.get("q")
    db = mysql.connect()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM film WHERE naziv LIKE %s", (f"%{rezultatPretrage}%"))
    
    result = cursor.fetchall()

    return flask.jsonify(result)

#user and admin stuff

# @app.route("/api/login", methods=['GET', 'POST'])
# def prijavljivanje():
#     data = request.json
#     username = data.get('korisnicko_ime')
#     password = data.get('password')
#     role = data.get('role')

#     return flask.jsonify()

@app.route("/api/korisnici", methods=['GET'])
def dobavi_korisnike():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik")
    rows = cursor.fetchall()

    return flask.jsonify(rows)

@app.route("/api/administratore", methods=['GET'])
def dobavi_Administratore():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM administrator")
    rows = cursor.fetchall()

    return flask.jsonify(rows)

@app.route("/api/register", methods=['POST'])
def dodavanje_korisnika_admina():
    role_podatak = flask.request.json
    db = mysql.get_db()
    cursor = db.cursor()
    if role_podatak['role'] == 'user':
        cursor.execute("INSERT INTO korisnik(korisnicko_ime, password, ime, role, admin_id) VALUES(%(korisnicko_ime)s, %(password)s, %(ime)s, %(role)s, %(admin_id)s)", flask.request.json)
    elif role_podatak['role'] == 'admin':
        cursor.execute("INSERT INTO administrator(korisnicko_ime, password, ime, role) VALUES(%(korisnicko_ime)s, %(password)s, %(ime)s, %(role)s)", flask.request.json)

    db.commit()

    return flask.jsonify(role_podatak), 201

@app.route("/api/korisnik/<int:idKorisnik>", methods=['PUT'])
def izmeni_korisnika(idKorisnik):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["idKorisnik"] = idKorisnik
                              
    cursor.execute("UPDATE korisnik SET korisnicko_ime=%(korisnicko_ime)s, password=%(password)s, ime=%(ime)s, role=%(role)s WHERE idKorisnik=%(idKorisnik)s", data)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@app.route("/api/korisnik/<int:idKorisnik>", methods=['DELETE'])
def brisanje_korisnika(idKorisnik):
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("DELETE FROM korisnik WHERE idKorisnik=%s", (idKorisnik))
    db.commit()
    return "", 204

#Filmovi

@app.route("/api/filmovi", methods=['GET'])
def dobavi_filmove():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM webioskop.film")
    rows = cursor.fetchall()
    for row in rows:
        row["premijera"] = row["premijera"].isoformat()
    return flask.jsonify(rows)

@app.route("/api/film/<int:idFilm>", methods=['GET'])
def dobavi_film(idFilm):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM film WHERE id=%s", (idFilm))
    film = cursor.fetchone()
    film["premijera"] = film["premijera"].isoformat()

    if film is not None:
        return flask.jsonify(film)
    else:
        return "", 404

@app.route("/api/film/<int:idFilm>", methods=['DELETE'])
def brisanje_filma(idFilm):
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("DELETE FROM film WHERE idFilm=%s", (idFilm))
    db.commit()
    return "", 204

@app.route("/api/film/<int:idFilm>", methods=['PUT'])
def izmena_film(idFilm):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["idFilm"] = idFilm
    data["premijera"] = datetime.datetime.strptime(data["premijera"],"%Y-%m-%d")
                              
    cursor.execute("UPDATE film SET naziv=%(naziv)s, zanr=%(zanr)s, reziser=%(reziser)s, premijera=%(premijera)s WHERE idFilm=%(idFilm)s", data)
    db.commit()
    return "", 200

@app.route("/api/film", methods=['POST'])
def novi_film():
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["premijera"] = datetime.datetime.strptime(data["premijera"],"%Y-%m-%d")

    cursor.execute("INSERT INTO film(naziv, zanr, reziser, premijera) VALUES(%(naziv)s,  %(zanr)s, %(reziser)s, %(premijera)s)", data)
    db.commit()
    return flask.jsonify(flask.request.json), 201


#projekcije

@app.route("/api/projekcije", methods=['GET'])
def dobavi_projekcije():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM webioskop.projekcija")
    rows = cursor.fetchall()
    for row in rows:
        row["termin"] = row["termin"].isoformat()
    return flask.jsonify(rows)

@app.route("/api/projekcija", methods=['POST'])
def nova_projekcija():
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["termin"] = datetime.datetime.strptime(data["termin"],"%Y-%m-%d")

    cursor.execute("INSERT INTO projekcija(film_id, termin, cena) VALUES(%(film_id)s , %(termin)s, %(cena)s)", data)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@app.route("/api/projekcija/<int:idProjekcija>", methods=['PUT'])
def izmeni_projekciju(idProjekcija):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["idProjekcija"] = idProjekcija
    data["termin"] = datetime.datetime.strptime(data["termin"],"%Y-%m-%d")
                              
    cursor.execute("UPDATE projekcija SET film_id=%(film_id)s, termin=%(termin)s, cena=%(cena)s WHERE idProjekcija=%(idProjekcija)s", data)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@app.route("/api/projekcija/<int:idProjekcija>", methods=['DELETE'])
def brisanje_projekcije(idProjekcija):
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("DELETE FROM projekcija WHERE idProjekcija=%s", (idProjekcija))
    db.commit()
    return "", 204

#karte

@app.route("/api/karte", methods=['GET'])
def dobavi_karte():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT k.idKarte, k.datum_kupovine, p.termin, k.cena, kr.korisnicko_ime, f.naziv FROM karte k LEFT JOIN projekcija p ON k.projekcija_id = p.idProjekcija LEFT JOIN korisnik kr on k.korisnik_id = kr.idKorisnik LEFT JOIN film f ON p.film_id = f.idFilm")
    rows = cursor.fetchall()
    for row in rows:
        row["datum_kupovine"] = row["datum_kupovine"].isoformat()
        row["termin"] = row["termin"].isoformat()

    return flask.jsonify(rows)

@app.route("/api/karta", methods=['POST'])
def kupi_kartu():
    db = mysql.get_db()
    cursos = db.cursor()
    data = flask.request.json
    data["datum_kupovine"] = datetime.datetime.strptime(data["datum_kupovine"],"%Y-%m-%d")
    print(data)

    cursos.execute("INSERT INTO karte( admin_id , korisnik_id , projekcija_id, datum_kupovine, cena) VALUES(%(admin_id)s, %(korisnik_id)s , %(projekcija_id)s, %(datum_kupovine)s ,%(cena)s)", data)
    db.commit()
    return flask.jsonify(flask.request.json), 201


@app.route("/api/karta/<int:idKarte>", methods=['PUT'])
def izmeni_termin_karte(idKarte):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["idKarte"] = idKarte
    data["datum_kupovine"] = datetime.datetime.strptime(data["datum_kupovine"],"%Y-%m-%d")
                              
    cursor.execute("UPDATE karte SET datum_kupovine=%(datum_kupovine)s, cena=%(cena)s WHERE idKarte=%(idKarte)s", data)
    db.commit()
    return "", 200
    
@app.route("/api/karta/<int:idKarte>", methods=['DELETE'])
def brisanje_karte(idKarte):
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("DELETE FROM karte WHERE idkarte=%s", (idKarte))
    db.commit()
    return "", 204


if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)